﻿#pragma once
#include "PhysicsObject.h"
#include <glm\ext.hpp>

class RigidBody :
	public PhysicsObject
{
public:
	RigidBody() {};
	RigidBody(ShapeType a_shapeID, glm::vec2 a_position, glm::vec2 a_velocity, float a_rotation, float a_mass, float linearDrag = 1, float angularDrag = 1, float a_elasticity = 1, bool a_static = false);
	~RigidBody();

	virtual void FixedUpdate(glm::vec3 gravity, float timestep);
	virtual void Debug();
	void ApplyForce(glm::vec2 a_force);
	void ApplyForceDebug(glm::vec2 a_force, bool outToCon);
	void ApplyForceToActor(RigidBody* a_actor2, glm::vec2 a_force);

	void SetLinearDrag(float linearDrag);
	void SetAngularDrag();

	void StopVelocity()
	{
		m_vVelocity = glm::vec2(0);
	}

	void SetPosition(glm::vec2 a_position) { m_vPosition = a_position; }
	glm::vec2 GetPosition() { return m_vPosition; }
	float GetRotation() { return m_fRotation; }
	glm::vec2 GetVelocity() { return m_vVelocity; }
	float GetMass() { return m_fMass;  }
	glm::vec2 PositionAtTime(glm::vec2 a_fGravity, float a_fTime);
	float GetElasticity() { return m_elasticity; }
protected:
	glm::vec2 m_vPosition;
	glm::vec2 m_vVelocity;
	glm::vec2 m_initialVelocity;
	glm::vec2 m_vInitialPosition;
	float m_angularVelocity;
	float m_fRotation;
	float m_fMass;
	float m_linearDrag = 0;
	float m_angularDrag = 0;
	float m_elasticity;
	bool m_isStatic;
};

