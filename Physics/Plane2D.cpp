#include "Plane2D.h"
#include <Gizmos.h>
#include <iostream>

using glm::vec2;
using glm::vec4;
using aie::Gizmos;
Plane2D::Plane2D()
{
}

Plane2D::Plane2D(glm::vec2 normal, float distance)
	: PhysicsObject(ShapeType::PLANE)
{
	m_vNormal = normal;
	m_fDistance = distance;
}


Plane2D::~Plane2D()
{
}

void Plane2D::FixedUpdate(glm::vec3 gravity, float timeStep)
{

}

void Plane2D::MakeGizmo()
{
	float lineSegmentLength = 300;
	vec2 centerPoint = m_vNormal * m_fDistance;
	
	vec2 parallel(m_vNormal.y, -m_vNormal.x);
	vec4 colour(1, 1, 1, 1);
	vec2 start = centerPoint + (parallel * lineSegmentLength);
	vec2 end = centerPoint - (parallel * lineSegmentLength);

	Gizmos::add2DLine(start, end, colour);

}

void Plane2D::resetPosition()
{
	m_vNormal = glm::vec2(0);
}
