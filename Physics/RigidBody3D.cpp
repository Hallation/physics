#include "RigidBody3D.h"

#include <iostream>
RigidBody3D::RigidBody3D(ShapeType a_shapeID, glm::vec4 a_position, glm::vec3 a_velocity, glm::mat4 rotation, float a_mass)
	:PhysicsObject(a_shapeID)
{
	m_vPosition = a_position;
	m_mRotation = rotation;
	m_fMass = a_mass;
	ApplyForce(a_velocity);
	m_vInitialPosition = m_vPosition;
	m_vInitialVelocity = m_vVelocity;
}

RigidBody3D::~RigidBody3D()
{
}

void RigidBody3D::FixedUpdate(glm::vec3 gravity, float timestep)
{
	m_vVelocity += gravity * timestep;
	m_vPosition += glm::vec4(m_vVelocity,1) * timestep;
}

void RigidBody3D::Debug()
{
}

void RigidBody3D::ApplyForce(glm::vec3 a_force)
{
	//acceleration = force / mass
	m_vVelocity += a_force / m_fMass;
	std::cout << a_force.x << " " << a_force.y << " " << a_force.z << std::endl;
	std::cout << m_vVelocity.x << " " << m_vVelocity.y << " " << m_vVelocity.z << std::endl;
}

void RigidBody3D::ApplyForceToActor(RigidBody3D * a_actor2, glm::vec3 a_force)
{
	//equal and opposite 
	a_actor2->ApplyForce(a_force);
	ApplyForce(-a_force);
}

glm::vec4 RigidBody3D::PositionAtTime(glm::vec3 a_vGravity, float a_fTime)
{
	//float x = m_vInitialPosition.x + a_fTime * m_vInitialVelocity.x;
	float x = m_vInitialPosition.x + a_fTime * m_vInitialVelocity.x;
	float y = (1.f / 2.f * a_vGravity.y * a_fTime * a_fTime) + a_fTime * m_vInitialVelocity.y + m_vInitialPosition.y;
	float z = m_vInitialPosition.z + a_fTime * m_vInitialVelocity.z;
	//std::cout << x << " " << y << " " << std::endl;
	return glm::vec4(x, y, z, 1);
}
