#pragma once
#include "PhysicsScene.h"
class PhysicsScene3D :
	public PhysicsScene
{
public:
	PhysicsScene3D();
	~PhysicsScene3D();

	void Update(float deltatime);
	void ApplyForces(glm::vec3 a_force);
	void UpdateGizmos();
	void DebugScene();

	void SetGravity(const glm::vec3 a_gravity)
	{
		m_vGravity = a_gravity;
	}
	glm::vec3 GetGravity()
	{
		return m_vGravity;
	}
private:
	glm::vec3 m_vGravity;
};

