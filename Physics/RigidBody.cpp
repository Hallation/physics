﻿#include "RigidBody.h"
#include <iostream>

#define MIN_LINEAR_THRESHOLD 0.05f
RigidBody::RigidBody(ShapeType a_shapeID, glm::vec2 a_position, glm::vec2 a_velocity, float a_rotation, float a_mass, float a_linearDrag, float a_angularDrag, float a_elasticity, bool a_static)
	:PhysicsObject(a_shapeID)
{
	m_vPosition = a_position;
	m_vVelocity = a_velocity;
	m_fRotation = a_rotation;
	m_fMass = a_mass;
	m_initialVelocity = a_velocity;
	m_vInitialPosition = m_vPosition;

	m_angularDrag = a_angularDrag;
	m_linearDrag = a_linearDrag;
	m_isStatic = a_static;
	m_elasticity = a_elasticity;

}

RigidBody::~RigidBody()
{
}

void RigidBody::FixedUpdate(glm::vec3 gravity, float timestep)
{
	if (m_isStatic) 
	{
		//std::cout << "im static" << std::endl;
		return;
	}
	//apply the force of gravity
	ApplyForce(glm::vec2(gravity) * m_fMass * timestep);
	//ApplyForce(glm::vec2(gravity));
	m_vPosition += (m_vVelocity)* timestep;
	m_vVelocity *= 1 - timestep * m_linearDrag;
	//std::cout << m_linearDrag << std::endl;
	//system("cls");

	if (glm::length(m_vVelocity) < MIN_LINEAR_THRESHOLD)
	{
		m_vVelocity = glm::vec2(0);
	}
	//ApplyForce(gravity)
}

void RigidBody::Debug()
{

}

void RigidBody::ApplyForce(glm::vec2 a_force)
{
	//std::cout << "Force being applied to actor " << a_force.x << " " << a_force.y << std::endl;
	//force = mass & acceleration
	//acceleration = Force / mass
	m_vVelocity += a_force / m_fMass;

	if (m_initialVelocity.x == 0 && m_initialVelocity.y == 0)
	{
		m_initialVelocity = m_vVelocity;
	}
}

void RigidBody::ApplyForceDebug(glm::vec2 a_force, bool outToCon)
{
	if (outToCon) 
	{
		std::cout << "Force being applied to actor " << a_force.x << " " << a_force.y << std::endl;
	}
	//force = mass & acceleration
	//acceleration = Force / mass
	m_vVelocity += a_force / m_fMass;

	if (m_initialVelocity.x == 0 && m_initialVelocity.y == 0)
	{
		m_initialVelocity = m_vVelocity;
	}
}

void RigidBody::ApplyForceToActor(RigidBody * a_actor2, glm::vec2 a_force)
{
	//equal and opposite reaction 
	//std::cout << "Force being applied to both actors " << a_force.x << " " << a_force.y << std::endl;
	this->ApplyForce(-a_force);
	a_actor2->ApplyForce(a_force);
	
	//this->ApplyForceDebug(-a_force, true);
	//a_actor2->ApplyForceDebug(a_force, true);
}

void RigidBody::SetLinearDrag(float linearDrag)
{
	m_linearDrag = linearDrag;
}

void RigidBody::SetAngularDrag()
{

}

glm::vec2 RigidBody::PositionAtTime(glm::vec2 a_fGravity, float a_fTime)
{
	//Trajectory formula
	float x = m_vInitialPosition.x + a_fTime * m_initialVelocity.x;
	float y = (1.f / 2.f * a_fGravity.y * a_fTime * a_fTime) + a_fTime * m_initialVelocity.y + m_vInitialPosition.y;
	//std::cout << x << " " << y << " " << std::endl;
	return glm::vec2(x, y);
}
