#include "Sphere.h"
#include <Gizmos.h>

//Position, velocity, mass, radius, colour
Sphere::Sphere(glm::vec4 a_position, glm::vec3 a_velocity, float m_fMass, float a_radius, glm::vec4 a_colour)
	:RigidBody3D(ShapeType::SHPERE, a_position, a_velocity, glm::mat4(), m_fMass)
{
	m_fRadius = a_radius;
	m_vColour = a_colour;
}

Sphere::~Sphere()
{
}

void Sphere::MakeGizmo()
{
	aie::Gizmos::addSphere(glm::vec3(m_vPosition), m_fRadius, 20, 20, m_vColour);
}
