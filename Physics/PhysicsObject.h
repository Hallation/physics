#pragma once
#include <glm\ext.hpp>
enum ShapeType
{
	PLANE,
	SHPERE,
	BOX,
	SHAPETYPE_LAST_ITEM,
};
//virtual class
class PhysicsObject
{
protected:
	PhysicsObject() {};
	PhysicsObject(ShapeType a_shapeID) : m_shapeID(a_shapeID) {};

public:
	virtual void FixedUpdate(glm::vec3 gravity, float timeStep) = 0;
	virtual void Debug() {};
	virtual void MakeGizmo() {};
	virtual void resetPosition() {};
	ShapeType GetShapeID()
	{
		return m_shapeID;
	}

protected:
	ShapeType m_shapeID;
};

