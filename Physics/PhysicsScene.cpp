#include "PhysicsScene.h"
#include "PhysicsObject.h"
#include "Circle.h"
#include "Plane2D.h"
#include "Box.h"

#include <iostream>

#include <thread>
#include <chrono>

#define SHAPE_COUNT ShapeType::SHAPETYPE_LAST_ITEM
typedef bool(*fn)(PhysicsObject*, PhysicsObject*); //function ptr

using glm::vec2;
PhysicsScene::PhysicsScene()
{

}


PhysicsScene::~PhysicsScene()
{
	for (auto it : m_actors)
	{
		if (it != nullptr) {
			delete it;
			it = nullptr;
		}
	}
}

static fn collisionFunctionArray[] =
{
	PhysicsScene::plane2Plane,   PhysicsScene::plane2Sphere,  PhysicsScene::plane2Box,
	PhysicsScene::sphere2Plane,  PhysicsScene::sphere2Sphere, PhysicsScene::sphere2Box,
	PhysicsScene::box2Plane,     PhysicsScene::box2Sphere,    PhysicsScene::box2Box,
};


void PhysicsScene::addActor(PhysicsObject * a_actor)
{
	m_actors.push_back(a_actor);
}

void PhysicsScene::removeActor(PhysicsObject * a_actor)
{
	
}

void PhysicsScene::Update(float deltatime)
{
	for (auto it : m_actors)
	{
		it->FixedUpdate(glm::vec3(m_vGravity, 1), m_timeStep);
	}

}

void PhysicsScene::ApplyForces(glm::vec2 a_force)
{

}

void PhysicsScene::UpdateGizmos()
{
	for (auto it : m_actors)
	{
		it->MakeGizmo();
	}
}

void PhysicsScene::DebugScene()
{
}

void PhysicsScene::checkForCollision()
{
	int actorCount = m_actors.size();

	for (int outer = 0; outer < actorCount; outer++)
	{
		for (int inner = 0; inner < actorCount; inner++)
		{
			PhysicsObject* object1 = m_actors[outer];
			PhysicsObject* object2 = m_actors[inner];
			int shapeID1 = object1->GetShapeID();
			int shapeID2 = object2->GetShapeID();
			int functionIdx = (shapeID1 * (SHAPE_COUNT)) + shapeID2;
			fn collisionFucntionPtr = collisionFunctionArray[functionIdx];
			if (collisionFucntionPtr != nullptr)
			{
				collisionFucntionPtr(object1, object2);
			}
		}
	}
}
//---------------------------------------------------------------------------------------
//PLANE COLLISION-----------------------------------------------------------------------
bool PhysicsScene::plane2Plane(PhysicsObject* obj1, PhysicsObject* obj2)
{
	return false;
}

bool PhysicsScene::plane2Sphere(PhysicsObject* obj1, PhysicsObject* obj2)
{
	return sphere2Plane(obj2, obj1);
}

bool PhysicsScene::plane2Box(PhysicsObject* obj1, PhysicsObject* obj2)
{
	return box2Plane(obj2, obj1);
}
//---------------------------------------------------------------------------------------
//SPHERE COLLISION-----------------------------------------------------------------------
bool PhysicsScene::sphere2Plane(PhysicsObject* obj1, PhysicsObject* obj2)
{
	Circle *sphere = dynamic_cast<Circle*>(obj1);
	Plane2D *plane = dynamic_cast<Plane2D*>(obj2);
	if (!sphere->ignoreCollisions) {
		if (sphere != nullptr && plane != nullptr)
		{
			glm::vec2 collisionNormal = plane->GetNormal();
			float sphereToPlane = glm::dot(sphere->GetPosition(), plane->GetNormal()) - plane->GetDistance();

			if (sphereToPlane < 0)
			{
				collisionNormal *= -1;
				sphereToPlane *= -1;
			}

			float intersection = sphere->getRadius() - sphereToPlane;
			if (intersection > 0)
			{
				vec2 planeNormal = plane->GetNormal();
				if (sphereToPlane < 0)
				{
					planeNormal *= -1;
				}
				vec2 forceVector = -1 * sphere->GetMass() * planeNormal * (glm::dot(planeNormal, sphere->GetVelocity()));

				float totalElasticity = sphere->GetElasticity() / 2.0f;
				sphere->ApplyForce(forceVector + (forceVector *  totalElasticity));

				vec2 seperationVector = collisionNormal * intersection * 0.5f;

				sphere->SetPosition(sphere->GetPosition() + (collisionNormal * intersection));
				return true;
				//sphere->StopVelocity();
			}

		}
	}
	return false;
}

bool PhysicsScene::sphere2Sphere(PhysicsObject* obj1, PhysicsObject* obj2)
{
	Circle* sphere1 = dynamic_cast<Circle*>(obj1);
	Circle* sphere2 = dynamic_cast<Circle*>(obj2);
	if (!sphere1->ignoreCollisions && !sphere2->ignoreCollisions)
	{
		if (sphere1 != nullptr && sphere2 != nullptr)
		{
			//D2 < (R1 + R2)
			//D2 = P2 - P1
			vec2 delta = sphere2->GetPosition() - sphere1->GetPosition();
			float distance = glm::length(delta);
			float intersection = sphere1->getRadius() + sphere2->getRadius() - distance;

			//std::cout << sphere2->GetPosition().x - sphere1->GetPosition().x << " " << sphere2->GetPosition().y - sphere1->GetPosition().y <<  std::endl;
			if (intersection > 0)
			{
				//std::cout << distance << " ";
				if (sphere1 != sphere2)
				{
					//sphere1->StopVelocity();
					//sphere2->StopVelocity();
					//sphere1->ApplyForceToActor(sphere2, glm::vec2(-0.5f, 0));

					vec2 collisionNormal = glm::normalize(sphere2->GetPosition() - sphere1->GetPosition());
					vec2 relativeVelocity = sphere1->GetVelocity() - sphere2->GetVelocity();
					vec2 collisionVector = collisionNormal * (glm::dot(relativeVelocity, collisionNormal));
					vec2 forceVector = collisionVector * 1.0f / (1.0f / sphere1->GetMass() + 1.0f / sphere2->GetMass());

					float combinedElasticity = (sphere1->GetElasticity() + sphere2->GetElasticity()) / 2.0f;
					std::cout << "Elasticity of sphere 1: " << sphere1->GetElasticity() << " Elasticity of sphere 2: " << sphere2->GetElasticity() << std::endl;
					sphere1->ApplyForceToActor(sphere2, forceVector + (forceVector * combinedElasticity));
					//sphere1->ApplyForce(-(forceVector + (forceVector * combinedElasticity)));
					//sphere2->ApplyForce(forceVector + (forceVector * combinedElasticity));
					//std::cout << "Applying forces to both actors" << std::endl;
					//sphere1->ApplyForce(-forcestoapply);
					//sphere2->ApplyForce(forcestoapply);
					//std::cout << "Forces applied to both actors" << std::endl;
					float intersection = sphere1->getRadius() + sphere2->getRadius() - distance;

					vec2 seperationVector = collisionNormal * intersection * 0.5f;

					sphere1->SetPosition(sphere1->GetPosition() - seperationVector);
					sphere2->SetPosition(sphere2->GetPosition() + seperationVector);
					//sphere2->ApplyForceToActor(sphere1, sphere2->GetVelocity());
					return true;
					std::cout << "Sphere Collision Detected" << std::endl;
				}
			}
		}
	}
	return false;
}

bool PhysicsScene::sphere2Box(PhysicsObject* obj1, PhysicsObject* obj2)
{
	Circle* sphere = dynamic_cast<Circle*>(obj1);
	Box* box = dynamic_cast<Box*>(obj2);

	if (!sphere->ignoreCollisions)
	{
		vec2 center = sphere->GetPosition();
		vec2 min = vec2(box->GetPosition().x - box->GetLength(), box->GetPosition().y - box->GetHeight());
		vec2 max = vec2(box->GetPosition().x + box->GetLength(), box->GetPosition().y + box->GetHeight());
		float distance = 0.0f;

		if (center.x + sphere->getRadius() < min.x)
		{
			distance += pow((center.x - sphere->getRadius()) - min.x, 2);
		}
		else if (center.x - sphere->getRadius() > max.x)
		{
			distance += pow((center.x + sphere->getRadius()) - max.x, 2);
		}

		if (center.y + sphere->getRadius() < min.y)
		{
			distance += pow((center.y - sphere->getRadius()) - min.y, 2);
		}
		else if (center.y - sphere->getRadius() > max.y)
		{
			distance += pow((center.y + sphere->getRadius()) - max.y, 2);
		}

		if (distance <= 0)
		{
			//sphere->StopVelocity();
			//box->StopVelocity();
			vec2 collisionNormal = glm::normalize(box->GetPosition() - sphere->GetPosition());
			//collisionNormal = glm::normalize(sphere->GetPosition() - box->GetPosition());
			vec2 relativeVelocity = sphere->GetVelocity() - box->GetVelocity();
			vec2 collisionVector = collisionNormal * (glm::dot(relativeVelocity, collisionNormal));

			vec2 forceVector = collisionVector * 1.0f / (1.0f / sphere->GetMass() + 1.0f / box->GetMass());

			float totalElasticity = (sphere->GetElasticity() + box->GetElasticity()) / 2.0f;
			sphere->ApplyForceToActor(box, forceVector + (forceVector * totalElasticity));

			//sphere->ApplyForce(forceVector * -2.0f);
			//box->ApplyForce(forceVector * 2.0f);

			float intersection = sphere->getRadius() - distance;
			vec2 seperationVector = collisionNormal * 0.75f;

			sphere->SetPosition(sphere->GetPosition() - seperationVector);
			box->SetPosition(box->GetPosition() + seperationVector);
			//std::cout << "Collision" << std::endl;
			//sphere->StopVelocity();
			//collision
		}
	}
	return false;
}
//---------------------------------------------------------------------------------------
//BOX COLLISION-----------------------------------------------------------------------
bool PhysicsScene::box2Plane(PhysicsObject* obj1, PhysicsObject* obj2)
{
	Box* box1 = dynamic_cast<Box*>(obj1);
	Plane2D* plane = dynamic_cast<Plane2D*>(obj2);

	//making a radius
	//where R = e0
	//aabb
	vec2 min = vec2(box1->GetPosition().x - box1->GetLength(), box1->GetPosition().y - box1->GetHeight());
	vec2 max = vec2(box1->GetPosition().x + box1->GetLength(), box1->GetPosition().y + box1->GetHeight());

	glm::vec2 E = (max - min) / 2.0f;
	glm::vec2 center = min + E;
	glm::vec2 N = plane->GetNormal();

	float radius = glm::abs(N.x* E.x) + abs(N.y * E.y);

	glm::vec2 A = glm::vec2((box1->GetPosition().x + box1->GetLength(), box1->GetPosition().y + box1->GetHeight()) * 0.5f);
	glm::vec2 B = plane->GetNormal() * plane->GetDistance();

	float boxToPlane = glm::dot(box1->GetPosition(), plane->GetNormal() - plane->GetDistance());
	float intersection = radius - boxToPlane;

	if (intersection > 0)
	{
		//we have collision
		vec2 planeNormal = plane->GetNormal();

		if (boxToPlane < 0)
		{
			planeNormal *= -1;
		}
		vec2 forceVector = -1 * box1->GetMass() * planeNormal * (glm::dot(planeNormal, box1->GetVelocity()));

		box1->ApplyForce(forceVector * 2.0f);

		vec2 seperationVector = planeNormal * intersection;

		box1->SetPosition(box1->GetPosition() + seperationVector);

		return true;
	}

	return false;
}

bool PhysicsScene::box2Box(PhysicsObject* obj1, PhysicsObject* obj2)
{
	if (obj1 != obj2)
	{
		//aabb
		Box* box1 = dynamic_cast<Box*>(obj1);
		Box* box2 = dynamic_cast<Box*>(obj2);

		glm::vec2 A = box1->GetPosition();
		glm::vec2 B = box2->GetPosition();

		vec2 Amin = vec2(box1->GetPosition().x - box1->GetLength(), box1->GetPosition().y - box1->GetHeight());
		vec2 Amax = vec2(box1->GetPosition().x + box1->GetLength(), box1->GetPosition().y + box1->GetHeight());

		vec2 Bmin = vec2(box2->GetPosition().x - box2->GetLength(), box2->GetPosition().y - box2->GetHeight());
		vec2 Bmax = vec2(box2->GetPosition().x + box2->GetLength(), box2->GetPosition().y + box2->GetHeight());

		if (Amax.x < Bmin.x || Amin.x > Bmax.x) return false;
		if (Amax.y < Bmin.y || Amin.y > Bmax.y) return false;

		vec2 collisionNormal = glm::normalize(box2->GetPosition() - box1->GetPosition());
		vec2 relativeVelocity = box2->GetVelocity() - box1->GetVelocity();

		vec2 collisionVector = collisionNormal * (glm::dot(relativeVelocity, collisionNormal));
		vec2 forceVector = collisionVector * 1.0f / (1.0f / box2->GetMass() + 1.0f / box1->GetMass());
		float totalElasticity = box1->GetElasticity() + box2->GetElasticity() / 2.0f;
		box2->ApplyForceToActor(box1, forceVector + (forceVector * totalElasticity));

		vec2 seperationVector = collisionNormal * 0.2f;

		box1->SetPosition(box1->GetPosition() - seperationVector);
		box2->SetPosition(box2->GetPosition() + seperationVector);
	}
	return false;

}

bool PhysicsScene::box2Sphere(PhysicsObject* obj1, PhysicsObject* obj2)
{
	return sphere2Box(obj2, obj1);
}
//---------------------------------------------------------------------------------------
