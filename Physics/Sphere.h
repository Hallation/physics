#pragma once
#include "RigidBody3D.h"
class Sphere :
	public RigidBody3D
{
public:
	Sphere() {};
	Sphere(glm::vec4 a_position, glm::vec3 a_velocity, float m_fMass, float a_radius, glm::vec4 a_colour);
	~Sphere();

	virtual void MakeGizmo();
	float GetRadius()
	{
		return m_fRadius;
	}

	glm::vec4 getColour()
	{
		return m_vColour;
	}

protected:
	float m_fRadius;
	glm::vec4 m_vColour;

};

