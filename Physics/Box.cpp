#include "Box.h"

#include <Gizmos.h>

Box::Box()
{
}

Box::Box(glm::vec2 a_position, glm::vec2 a_velocity, float a_mass, int a_height, int a_length, glm::vec4 a_colour)
	:RigidBody(ShapeType::BOX, a_position, a_velocity, 0, a_mass)
{
	m_iHeight = a_height;
	m_iLength = a_length;
	m_vColour = a_colour;
}


Box::~Box()
{
}

void Box::MakeGizmo()
{
	aie::Gizmos::add2DAABB(m_vPosition, glm::vec2(m_iLength, m_iHeight), m_vColour);
	//aie::Gizmos::add2DCircle(m_vPosition, 0.2f, 10, glm::vec4(1));
	//aie::Gizmos::add2DLine(glm::vec2(m_vPosition.x , m_vPosition.y), glm::vec2(m_vPosition.x + m_iLength, m_vPosition.y), glm::vec4(1));
	//aie::Gizmos::add2DLine(glm::vec2(m_vPosition.x, m_vPosition.y), glm::vec2(m_vPosition.x , m_vPosition.y + m_iHeight), glm::vec4(1));
}
