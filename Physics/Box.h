#pragma once
#include "RigidBody.h"
class Box :
	public RigidBody
{
public:
	Box();
	Box(glm::vec2 a_position, glm::vec2 a_velocity, float a_mass, int length, int height, glm::vec4 a_colour);
	~Box();


	void MakeGizmo();
	int GetLength()
	{
		return m_iLength;
	}
	int GetHeight()
	{
		return m_iHeight;
	}
	glm::vec4 getColour()
	{
		return m_vColour;
	}

protected:
	int m_iLength;
	int m_iHeight;

	glm::vec4 m_vColour;
};

