#include "PhysicsScene3D.h"

#include "PhysicsObject.h"

PhysicsScene3D::PhysicsScene3D()
{
}


PhysicsScene3D::~PhysicsScene3D()
{
	PhysicsScene::~PhysicsScene();
}

void PhysicsScene3D::Update(float deltatime)
{
	for (auto it : m_actors)
	{
		it->FixedUpdate(m_vGravity, m_timeStep);
	}
}

void PhysicsScene3D::ApplyForces(glm::vec3 a_force)
{
}

void PhysicsScene3D::UpdateGizmos()
{
	for (auto it : m_actors)
	{
		it->MakeGizmo();
	}
}

void PhysicsScene3D::DebugScene()
{
}
