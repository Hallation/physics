#pragma once
#include "PhysicsObject.h"
class Plane2D :
	public PhysicsObject
{
public:
	Plane2D();
	Plane2D(glm::vec2 normal, float distance);
	~Plane2D();

	virtual void FixedUpdate(glm::vec3 gravity, float timeStep);
	virtual void Debug() {};
	virtual void MakeGizmo();
	virtual void resetPosition();

	glm::vec2 GetNormal()
	{
		return m_vNormal;
	}
	float GetDistance()
	{
		return m_fDistance;
	}
	glm::vec2 m_vNormal;
protected:
	float m_fDistance;
};

