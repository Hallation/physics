#include "Circle.h"
#include <Gizmos.h>

#include <iostream>
Circle::Circle(glm::vec2 a_position, glm::vec2 a_velocity, float a_mass, float a_radius, glm::vec4 a_colour, float a_linearDrag, float a_angularDrag, float a_elasticity, bool a_isStatic, bool a_ignoreCollisions)
	: RigidBody(ShapeType::SHPERE, a_position, a_velocity,0, a_mass, a_linearDrag, a_angularDrag, a_elasticity, a_isStatic)
{
	m_fRadius = a_radius;
	m_vColour = a_colour;
	ignoreCollisions = a_ignoreCollisions;
}

Circle::~Circle()
{

}

void Circle::MakeGizmo()
{
	aie::Gizmos::add2DCircle(m_vPosition, m_fRadius, 50, m_vColour);
	//aie::Gizmos::addSphere(glm::vec3(m_vPosition, 1), m_fRadius, 50, 50, m_vColour);
}

void Circle::AddMass(float aMass)
{
	float temp = m_fMass;
	temp += aMass;
	m_fMass = temp;
}
