#pragma once
#include "RigidBody.h"

class Circle :
	public RigidBody
{
public:
	Circle() {};
	Circle(glm::vec2 a_position, glm::vec2 a_velocity, float a_mass, float a_radius, glm::vec4 a_colour, float a_linearDrag = 1, float a_angularDrag = 1, float a_elasticity = 1, bool a_isStatic = false, bool a_ignoreCollisions = false);
	~Circle();

	virtual void MakeGizmo();

	void AddMass(float aMass);
	
	float getRadius()
	{
		return m_fRadius;
	}
	glm::vec4 getColour()
	{
		return m_vColour;
	}
	bool ignoreCollisions;
protected:
	float m_fRadius;
	glm::vec4 m_vColour;
};

