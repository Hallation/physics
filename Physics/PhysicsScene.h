#pragma once
#include <glm\ext.hpp>
#include <vector>
#include <atomic>
class PhysicsObject;
class PhysicsScene
{
public:
	PhysicsScene();
	~PhysicsScene();

	void addActor(PhysicsObject* a_actor);
	void removeActor(PhysicsObject* a_actor);

	virtual void Update(float deltatime);
	virtual void ApplyForces(glm::vec2 a_force);
	virtual void UpdateGizmos();
	virtual void DebugScene();

	void SetGravity(const glm::vec2 a_gravity)
	{
		m_vGravity = a_gravity;
	}

	void SetTimeStep(const float timeStep) { m_timeStep = timeStep; }
	float GetTimeStep() const { return m_timeStep; }
	glm::vec2 GetGravity() const { return m_vGravity; }

	void checkForCollision();
	//plane colliisons
	static bool plane2Plane(PhysicsObject* obj1, PhysicsObject* obj2);
	static bool plane2Sphere(PhysicsObject* obj1, PhysicsObject* obj2);
	static bool plane2Box(PhysicsObject* obj1, PhysicsObject* obj2);

	//sphere collisions
	static bool sphere2Plane(PhysicsObject* obj1, PhysicsObject* obj2);
	static bool sphere2Sphere(PhysicsObject* obj1, PhysicsObject* obj2);
	static bool sphere2Box(PhysicsObject* obj1, PhysicsObject* obj2);
	
	//box collisions
	static bool box2Plane(PhysicsObject* obj1, PhysicsObject* obj2);
	static bool box2Box(PhysicsObject* obj1, PhysicsObject* obj2);
	static bool box2Sphere(PhysicsObject* obj1, PhysicsObject* obj2);


protected:
	glm::vec2 m_vGravity;
	float m_timeStep;
	std::vector<PhysicsObject*> m_actors;

};

