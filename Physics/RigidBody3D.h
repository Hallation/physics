#pragma once
#include "PhysicsObject.h"
class RigidBody3D :
	public PhysicsObject
{
public:
	RigidBody3D() {};
	RigidBody3D(ShapeType a_shapeID, glm::vec4 a_position, glm::vec3 a_velocity, glm::mat4 rotation, float a_mass);
	~RigidBody3D();

	virtual void FixedUpdate(glm::vec3 gravity, float timestep);
	virtual void Debug();
	void ApplyForce(glm::vec3 a_force);
	void ApplyForceToActor(RigidBody3D* a_actor2, glm::vec3 a_force);

	glm::vec4 PositionAtTime(glm::vec3 a_vGravity, float a_fTime);
protected:
	glm::vec4 m_vPosition;
	glm::vec3 m_vVelocity;
	glm::vec4 m_vInitialPosition;
	glm::vec3 m_vInitialVelocity;

	glm::mat4 m_mRotation;
	float m_fMass;

};

