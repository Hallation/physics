﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRaycast : MonoBehaviour
{

    public Camera _camera;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = _camera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray , out hit))
            {
                if (hit.transform.tag == "Player")
                    hit.transform.gameObject.GetComponent<Ragdoll>().RagdollMe();
            }
            Debug.DrawRay(ray.origin , ray.direction , Color.magenta , 5.0f);
        }
    }
}
