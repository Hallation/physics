﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{

    CharacterController m_characterController;

    public Vector3 gravity = new Vector3(0 , -9.8f , 0);

    public float m_fMass = 1;
    public float m_fDrag;
    public float firingForce;
    private Vector3 m_vVelocity;

    public GameObject bullet;
    void Start()
    {
        m_characterController = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (m_characterController.enabled)
        {
            if (Input.GetKey(KeyCode.W))
            {
                // m_vVelocity += this.transform.forward * Time.deltaTime;
                m_characterController.Move(Vector3.forward * 10 * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.S))
            {
                // m_vVelocity -= this.transform.forward * Time.deltaTime;
                m_characterController.Move(-Vector3.forward * 10 * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.A))
            {
                // m_vVelocity -= this.transform.right * Time.deltaTime;
                m_characterController.Move(-Vector3.right * 10 * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.D))
            {
                // m_vVelocity += this.transform.right * Time.deltaTime;
                m_characterController.Move(Vector3.right * 10 * Time.deltaTime);
            }

            if (Input.GetKey(KeyCode.Space))
            {
                GameObject go = Instantiate(bullet , this.transform.position + this.transform.forward * 2 + new Vector3(0 , 0.5f) , Quaternion.identity);
                go.GetComponent<Rigidbody>().AddRelativeForce(Vector3.forward * firingForce , ForceMode.VelocityChange);
            }
        }
    }

    private void FixedUpdate()
    {
        ApplyForce(gravity * m_fMass * Time.fixedDeltaTime);
        m_vVelocity *= 1 - Time.fixedDeltaTime * m_fDrag;
        if (m_characterController.enabled)
        {
            m_characterController.Move(m_vVelocity);
        }
    }

    private void ApplyForce(Vector3 a_force)
    {
        m_vVelocity += a_force / m_fMass;
    }

    void OnTriggerEnter(Collider a_collider)
    {
        if (a_collider.tag != "Player")
        {
            a_collider.GetComponent<CubeHeliControl>().DoHeliControls();
            a_collider.GetComponent<CubeHeliControl>().helicopterToControl.EmitParticle();
        }
    }

    void OnTriggerStay(Collider a_collider)
    {
        if (a_collider.tag != "Player")
        {
            a_collider.GetComponent<CubeHeliControl>().DoHeliControls();
        }
    }
    private void OnControllerColliderHit(ControllerColliderHit hit)
    {

    }
}
