﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddForce : MonoBehaviour {

	// Use this for initialization
	void Start ()
    {
        //GetComponent<Rigidbody>().AddForce(Vector3.forward * 10 , ForceMode.VelocityChange);
	}
	
	// Update is called once per frame
	void Update ()
    {
		if (Input.GetKey(KeyCode.O))
        {
            foreach (Rigidbody bone in GetComponentsInChildren<Rigidbody>())
            {
                bone.AddForce(Vector3.forward * 10 , ForceMode.VelocityChange);
            }
            //GetComponent<Rigidbody>().AddForce(Vector3.forward * 10 );
        }
        Debug.DrawRay(this.transform.position , this.transform.forward, Color.blue, 10);
	}
}
