﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ControlType
{ 
    POSITIVE_THROTTLE,
    NEGATIVE_THROTTLE,
    POSITIVE_PITCH,
    NEGATIVE_PITCH,
    POSITIVE_ROLL,
    NEGATIVE_ROLL,
    POSITIVE_YAW,
    NEGATIVE_YAW,
}

public class CubeHeliControl : MonoBehaviour
{
    public HelicopterControl helicopterToControl;
    public ControlType m_heliControlType;
	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void DoHeliControls()
    {

        switch(m_heliControlType)
        {
            case ControlType.POSITIVE_THROTTLE:
                helicopterToControl.AddThrottle(1.0f);
                break;
            case ControlType.NEGATIVE_THROTTLE:
                helicopterToControl.AddThrottle(-1.0f);
                break;
            case ControlType.POSITIVE_PITCH:
                helicopterToControl.AddPitchOrRoll(1.0f);
                break;
            case ControlType.NEGATIVE_PITCH:
                helicopterToControl.AddPitchOrRoll(-1.0f);
                break;
            case ControlType.POSITIVE_ROLL:
                helicopterToControl.AddPitchOrRoll(0 , 1.0f);
                break;
            case ControlType.NEGATIVE_ROLL:
                helicopterToControl.AddPitchOrRoll(0 , -1.0f);
                break;
            case ControlType.POSITIVE_YAW:
                helicopterToControl.AddYaw(1.0f);
                break;
            case ControlType.NEGATIVE_YAW:
                helicopterToControl.AddYaw(-1.0f);
                break;
        }
    }
}
