﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class HelicopterControl : MonoBehaviour
{

    public GameObject mainRotor;
    public GameObject tailRotor;
    public Transform CenterOfMass;
    public float max_Rotor_Force = 22241.1081f;
    public float max_Rotor_Velocity = 7200;
    public float rotor_Velocity = 0;
    private float rotor_Rotation = 0;

    public float max_tail_Rotor_Force = 15000;
    public float max_Tail_Rotor_Velocity = 2200;
    public float tail_Rotor_Velocity = 0;
    private float tail_Rotor_Rotation = 0;

    public float forward_Rotor_Torque_Multiplier = 0.5f; //multipliers for control input
    public float sideways_Rotor_Torque_Multiplier = 0.5f;

    public bool main_rotor_active = true;
    public bool tail_rotor_active = true;

    private Rigidbody _rigidbody;

    private float m_yawAmount = 0.0f;
    private ParticleSystem _particleSystem;
    // Use this for initialization
    void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
        //_rigidbody.ResetCenterOfMass();
        _rigidbody.centerOfMass = CenterOfMass.localPosition;
        _particleSystem = GetComponentInChildren<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        _rigidbody.centerOfMass = CenterOfMass.localPosition;
        tail_Rotor_Rotation += max_Tail_Rotor_Velocity * tail_Rotor_Velocity;

        rotor_Rotation += max_Rotor_Velocity * rotor_Velocity;

        if (main_rotor_active)
        {
            mainRotor.transform.rotation = transform.rotation * Quaternion.Euler(-90 , rotor_Rotation , 0);
        }
        if (tail_rotor_active)
        {
            tailRotor.transform.rotation = transform.rotation * Quaternion.Euler(0 , 90 , tail_Rotor_Rotation);
        }

        if (tail_Rotor_Rotation >= 18000)
        {
            tail_Rotor_Rotation -= max_tail_Rotor_Force * tail_Rotor_Velocity;
        }

        var hover_Rotor_velocity = (_rigidbody.mass * Mathf.Abs(Physics.gravity.y) / max_Rotor_Force);
        var hover_Tail_Rotor_velocity = (max_Rotor_Force * rotor_Velocity) / max_tail_Rotor_Force;

        //if (rotor_Velocity <= 0.0f)
        //{
        //    main_rotor_active = false;
        //}

        if (Input.GetAxis("Throttle") != 0.0)
        {
            rotor_Velocity += (/*Input.GetAxis("Throttle"))*/ 0 * 0.001f * Time.deltaTime);
        }
        else
        {
            rotor_Velocity = Mathf.Lerp(rotor_Velocity , hover_Rotor_velocity , Time.deltaTime * Time.deltaTime * 5);
        }

        if (Input.GetAxis("Yaw") != 0.0f)
        {
            Debug.Log(Input.GetAxis("Yaw"));
        }
        tail_Rotor_Velocity = hover_Tail_Rotor_velocity - /*Input.GetAxis("Yaw")*/ m_yawAmount * Time.deltaTime;
        if (rotor_Velocity > 1.0)
        {
            rotor_Velocity = 1.0f;
        }
        else if (rotor_Velocity < 0.0)
        {
            rotor_Velocity = 0;
        }
        m_yawAmount = 0;
    }


    private void FixedUpdate()
    {
        Vector3 torqueValue = Vector3.zero;
        Vector3 controlTorque = controlTorque = new Vector3(/*-Input.GetAxis("Pitch")*/ 0 * forward_Rotor_Torque_Multiplier , 1.0f , /*Input.GetAxis("Roll")*/ 0 * sideways_Rotor_Torque_Multiplier);
        if (main_rotor_active == true)
        {
            torqueValue += (controlTorque * max_Rotor_Force * (rotor_Velocity));
            _rigidbody.AddRelativeForce(Vector3.up * max_Rotor_Force * rotor_Velocity);
        };

        if (Vector3.Angle(Vector3.up , transform.up) < 80)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation , Quaternion.Euler(0 , transform.rotation.eulerAngles.y , 0) , Time.deltaTime * rotor_Velocity * 2);
        }

        if (tail_rotor_active)
        {
            torqueValue -= (Vector3.up * max_tail_Rotor_Force * (tail_Rotor_Velocity));
        }

        _rigidbody.AddRelativeTorque(torqueValue);
    }


    public void AddThrottle(float throttleAmount)
    {
        rotor_Velocity += throttleAmount * 0.001f * Time.deltaTime;

    }

    public void AddPitchOrRoll(float pitchAmount = 0, float rollAmount = 0)
    {
        Vector3 torqueValue = Vector3.zero;
        Vector3 controlTorque = controlTorque = new Vector3(pitchAmount * forward_Rotor_Torque_Multiplier , 1.0f , rollAmount * sideways_Rotor_Torque_Multiplier);
        if (main_rotor_active == true)
        {
            torqueValue += (controlTorque * max_Rotor_Force * (rotor_Velocity));
            _rigidbody.AddRelativeForce(Vector3.up * max_Rotor_Force * rotor_Velocity);
        };

        if (Vector3.Angle(Vector3.up , transform.up) < 80)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation , Quaternion.Euler(0 , transform.rotation.eulerAngles.y , 0) , Time.deltaTime * rotor_Velocity * 2);
        }

        if (tail_rotor_active)
        {
            torqueValue -= (Vector3.up * max_tail_Rotor_Force * (tail_Rotor_Velocity));
        }

        _rigidbody.AddRelativeTorque(torqueValue);
    }

    public void AddYaw(float yawAmount)
    {
        //var hover_Tail_Rotor_velocity = (max_Rotor_Force * rotor_Velocity) / max_tail_Rotor_Force;
        m_yawAmount = yawAmount;
    }

    public void EmitParticle()
    {
        _particleSystem.Play();
    }
}
