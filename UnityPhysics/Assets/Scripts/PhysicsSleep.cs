﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PhysicsSleep : MonoBehaviour {

    // Use this for initialization
    public Material awakeMat = null;
    public Material sleepMat = null;

    private Rigidbody _rigibody = null;

    bool wasSleeping = false;

    public float mTorque = 0;
    
	void Start ()
    {
        _rigibody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    private void FixedUpdate()
    {
        if (_rigibody.IsSleeping() && !wasSleeping && sleepMat != null)
        {
            wasSleeping = true;
            GetComponent<MeshRenderer>().material = sleepMat;
        }
        if (!_rigibody.IsSleeping() && wasSleeping && awakeMat != null)
        {
            wasSleeping = false;
            GetComponent<MeshRenderer>().material = awakeMat;
        }

        _rigibody.AddTorque(new Vector3(0 , mTorque , 0),ForceMode.Acceleration);

    }
}
