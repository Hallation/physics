﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ragdoll : MonoBehaviour
{


    Rigidbody[] bones;
    public GameObject _ragdoll;

    // Use this for initialization
    void Start()
    {
        bones = GetComponentsInChildren<Rigidbody>();
        foreach (Rigidbody bone in bones)
        {
            bone.isKinematic = true;
        }

    }

    // Update is called once per frame
    void Update()
    {
        //ragdoll myself
        if (Input.GetKey(KeyCode.Y))
        {
            RagdollMe();
        }

    }

    public void RagdollMe()
    {
        foreach (Rigidbody bone in bones)
        {
            bone.isKinematic = false;
            this.GetComponent<CharacterController>().enabled = false;
        }
        this.GetComponentInChildren<ParticleSystem>().Play();
        this.GetComponent<CapsuleCollider>().enabled = false;
    }


}
