﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileRaycast : MonoBehaviour {

	// Use this for initialization
	void Start ()
    {
        Destroy(this.gameObject , 5);
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    void FixedUpdate()
    {
        Vector3 rayOrigin = this.transform.position;
        Vector3 rayDirection = this.transform.forward;
        float RaycastDistance = 1.0f;
        RaycastHit hit;

        Debug.DrawRay(rayOrigin , rayDirection , Color.cyan , 1.0f);

        if (Physics.Raycast(rayOrigin, rayDirection, out hit, RaycastDistance, 1 << 8))
        {
            Debug.Log("raycast hit something");
            hit.transform.gameObject.GetComponent<CubeHeliControl>().DoHeliControls();
            Destroy(this.gameObject);
        }
    }
}
