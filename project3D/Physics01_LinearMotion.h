#pragma once
#include "Application3D.h"
class PhysicsScene3D;
class Sphere;
class Physics01_LinearMotion :
	public Application3D
{
public:
	Physics01_LinearMotion();
	~Physics01_LinearMotion();

	virtual bool startup();
	virtual void shutdown();

	virtual void update(float dt);
	virtual void draw();

protected:
	PhysicsScene3D* m_physicsScene;
	Sphere* ball1;
	Sphere* ball2;
	Sphere* projectileSphere;
};

