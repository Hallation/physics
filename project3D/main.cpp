#include "Application3D.h"
#include "Physics01_LinearMotion.h"
int main() {
	
	// allocation
	auto app = new Physics01_LinearMotion();

	// initialise and loop
	app->run("AIE", 1280, 720, false);

	// deallocation
	delete app;

	return 0;
}