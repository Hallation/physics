#include "Physics01_LinearMotion.h"

#include <PhysicsScene3D.h>
#include <Sphere.h>
#include <glm\ext.hpp>
#include <Gizmos.h>
#include <Input.h>
#include <imgui.h>

using aie::Gizmos;
using glm::vec2;
using glm::vec3;
using glm::vec4;
using glm::mat4;
Physics01_LinearMotion::Physics01_LinearMotion()
{

}


Physics01_LinearMotion::~Physics01_LinearMotion()
{
}

bool Physics01_LinearMotion::startup()
{
	setBackgroundColour(0.25f, 0.25f, 0.25f);

	// initialise gizmo primitive counts
	Gizmos::create(255U, 100000, 100000, 100000);

	// create simple camera transforms

	mCamera = new FPSCamera();
	glm::vec3 camPos(10, 10, 10);
	glm::vec3 camLookAt(0);
	glm::vec3 camUp(0, 1, 0);
	mCamera->SetviewTransform(camPos, camLookAt, camUp);
	mCamera->SetProjectionTransform(glm::pi<float>() * 0.25f, 16 / 9.f, 1.f, 2000.f);
	
	m_physicsScene = new PhysicsScene3D();
	m_physicsScene->SetGravity(glm::vec3(0, -9.8f, 0));
	m_physicsScene->SetTimeStep(0.1f);

	//ball1 = new Sphere(glm::vec4(0, 0, 0, 1), glm::vec3(0, 0, 0), 5.0f, 1, glm::vec4(0, 1, 0, 1));
	//ball2 = new Sphere(glm::vec4(0, 0, 0, 1), glm::vec3(0, 0, 0), 5.0f, 1, glm::vec4(1, 1, 0, 1));
	projectileSphere = new Sphere(glm::vec4(0.2f, 0.8f, 0.2f, 1), glm::vec3(-10, 20, -50), 2.f, 0.2f, glm::vec4(1, 0, 0 , 1));
	
	//m_physicsScene->addActor(ball1);
	//m_physicsScene->addActor(ball2);
	
	float time = 60;

	for (float i = 0; i < time; i += /*m_physicsScene->GetTimeStep()*/0.09f)
	{
		//projectile->, 0.3f, 20, glm::vec4(0, 1, 0, 1);
		glm::vec4 predictionvector = projectileSphere->PositionAtTime(m_physicsScene->GetGravity(), i);
		Gizmos::addSphere(glm::vec3(predictionvector), 0.1f, 10, 10, glm::vec4(0, 1, 0, 0.5f));
	}

	return true;
}

void Physics01_LinearMotion::shutdown()
{
	
	Gizmos::destroy();
}

void Physics01_LinearMotion::update(float dt)
{
	m_physicsScene->Update(dt);
	m_physicsScene->UpdateGizmos();
	mCamera->Update(m_window, dt);
	aie::Input* input = aie::Input::getInstance();

	if (input->isKeyDown(aie::INPUT_KEY_J))
		m_physicsScene->addActor(projectileSphere);

	if (input->isKeyDown(aie::INPUT_KEY_ESCAPE))
		quit();
}

void Physics01_LinearMotion::draw()
{
	clearScreen();
	// update perspective in case window resized

	Gizmos::draw(mCamera->GetProjectionView());
}
