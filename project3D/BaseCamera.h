#pragma once
#include "glm\ext.hpp"
class BaseCamera
{
public:
	BaseCamera();
	~BaseCamera();
	virtual void Update(float deltatime);
	
	//setters
	void setPerspective(float aFOV, float afAspectRatio, float near, float far);
	void setLookat(glm::vec3 aFrom, glm::vec3 aTo ,glm::vec3 aUp);
	void setPosition(glm::vec3 aPosition);
	//getters
	glm::mat4 GetWorldTransform();
	glm::mat4 GetView();
	glm::mat4 GetProjection();
	glm::mat4 GetProjectionView();

	void UpdateProjectionViewTransform();
protected:

	glm::mat4 worldTransform;
	glm::mat4 viewTransform;
	glm::mat4 projectionTransform;
	glm::mat4 projectionViewTransform;

	float mfRadius;
	double mfCursorX;
	double mfCursorY;
	float mfLastCursorX;
	float mfLastCursorY;
	float mfPitch;
	float mfYaw;
	float mfRoll;

	bool mbFirstMouseMovement;

};

