#pragma once
#include "Application2D.h"
#include <vector>
#include <chrono>
#include <thread>

class PhysicsScene;
class Circle;
class Box;
class Plane2D;
class Rocket;
class Physics_Linearforce :
	public Application2D
{
public:
	Physics_Linearforce();
	~Physics_Linearforce();


	void RunPhysicsScene(float deltatime);

	virtual bool startup();
	virtual void shutdown();

	virtual void update(float deltaTime);
	virtual void draw();


	
protected:
	PhysicsScene* m_physicsScene;
	//these pointers are clearead by the physics scene
	Circle* ball;
	Circle* ball2;
	Circle* projectile;
	Circle* projectile2;
	Plane2D*plane;
	Rocket* m_Rocket;
	Box* box;

	bool m_bRunPhysics;

};

