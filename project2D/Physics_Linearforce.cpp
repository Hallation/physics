﻿#include "Physics_Linearforce.h"

#include "Texture.h"
#include "Font.h"
#include "Input.h"

#include "PhysicsScene.h"
#include "Circle.h"
#include "Box.h"
#include "Plane2D.h"
#include "Rocket.h"

#include <Gizmos.h>
#include <iostream>


Physics_Linearforce::Physics_Linearforce()
{

}


Physics_Linearforce::~Physics_Linearforce()
{
}

void Physics_Linearforce::RunPhysicsScene(float deltaTime)
{


}

bool Physics_Linearforce::startup()
{
	m_2dRenderer = new aie::Renderer2D();
	m_font = new aie::Font("./font/consolas.ttf", 32);
	m_audio = new aie::Audio("./audio/powerup.wav");

	m_cameraX = 0;
	m_cameraY = 0;
	m_timer = 0;
	aie::Gizmos::create(255U, 1000000, 1000000, 1000000);

	m_physicsScene = new PhysicsScene();
	m_physicsScene->SetGravity(glm::vec2(0, -9.8f));
	m_physicsScene->SetTimeStep(0.01f);

	ball = new Circle(glm::vec2(-70, -10), glm::vec2(0, 0), 2.0f, 1, glm::vec4(1, 0, 0, 1));
	ball2 = new Circle(glm::vec2(-68, -10), glm::vec2(0, 0), 2.0f, 1, glm::vec4(1, 1, 0, 1));
	projectile = new Circle(glm::vec2(-90, 10), glm::vec2(50, 10), 10.f, 2.f, glm::vec4(0.5f, 0.2f, 0.5f, 1), 0.f, 1.f, 1.2f);
	projectile2 = new Circle(glm::vec2(90, 10), glm::vec2(-50, 10), 10.f, 2.f, glm::vec4(1.f, 0.2f, 0.5f, 1), 0.f, 1.f, 1.2f);

	//projectile->ApplyForce(glm::vec2(1000, 10));
	//projectile2->ApplyForce(glm::vec2(-1000, 10));

	plane = new Plane2D(glm::vec2(0.f, 1.f), 0);
	m_Rocket = new Rocket(20, glm::vec2(-80, 20), glm::vec2(0), 300, 2.0f, glm::vec4(1, 1, 0, 1));

	box = new Box(glm::vec2(-20, 20), glm::vec2(20,0), 10, 10, 10, glm::vec4(1));
	Box* temp = new Box(glm::vec2(20, 20), glm::vec2(-20,0), 10, 10, 10, glm::vec4(1,0,0,1));
	m_physicsScene->addActor(temp);
	m_physicsScene->addActor(box);
	m_physicsScene->addActor(ball);
	m_physicsScene->addActor(ball2);
	m_physicsScene->addActor(projectile);
	m_physicsScene->addActor(projectile2);
	m_physicsScene->addActor(plane);
	m_physicsScene->addActor(m_Rocket);


	return true;
}

void Physics_Linearforce::shutdown()
{

	delete m_audio;
	delete m_font;
	delete m_2dRenderer;

	delete m_physicsScene;
	aie::Gizmos::destroy();

}

void Physics_Linearforce::update(float deltaTime)
{
	aie::Input* input = aie::Input::getInstance();

	aie::Gizmos::clear();


	m_physicsScene->UpdateGizmos();
	m_physicsScene->Update(0.1f);
	m_physicsScene->checkForCollision();


	m_Rocket->FUpdate(deltaTime, m_physicsScene);

	if (input->isKeyDown(aie::INPUT_KEY_ESCAPE))
		quit();

	if (input->isKeyDown(aie::INPUT_KEY_K)) {
		ball->ApplyForceToActor(ball2, glm::vec2(10, 0));
	}
	if (input->isKeyDown(aie::INPUT_KEY_UP)) {
		plane->m_vNormal.y += 0.005f * deltaTime;
	}
	if (input->isKeyDown(aie::INPUT_KEY_DOWN)) {
		plane->m_vNormal.y -= 0.005f * deltaTime;
	}

	//show the trajectory for the projectile
	float time = 10;
	for (float i = 0; i < time; i += 0.01f)
	{
		//projectile->, 0.3f, 20, glm::vec4(0, 1, 0, 1);
		glm::vec2 predictionvector = projectile->PositionAtTime(m_physicsScene->GetGravity(), i);
		aie::Gizmos::add2DCircle(predictionvector, 0.2f, 10, glm::vec4(0, 1, 0, 1));

		glm::vec2 prediction2 = projectile2->PositionAtTime(m_physicsScene->GetGravity(), i);
		aie::Gizmos::add2DCircle(prediction2, 0.2f, 10, glm::vec4(1, 1, 0, 1));
	}
	float mfSpeed = 10.0f;

}

void Physics_Linearforce::draw()
{
	clearScreen();
	m_2dRenderer->setCameraPos(m_cameraX, m_cameraY);
	m_2dRenderer->begin();

	//draw in here
	static float aspectRatio = 16 / 9.f;
	aie::Gizmos::draw2D(glm::ortho<float>(-100, 100, -100 / aspectRatio, 100 / aspectRatio, -1.0, 1.0f));

	m_2dRenderer->drawText(m_font, "Press ESC to quit", 0, 0);
	m_2dRenderer->end();
}


