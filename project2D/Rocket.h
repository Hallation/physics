#pragma once
#include <Circle.h>
class PhysicsScene;
class Rocket :
	public Circle
{
public:
	Rocket();
	Rocket(float a_Fuel, glm::vec2 a_position, glm::vec2 a_velocity, float a_mass, float a_radius, glm::vec4 a_colour);
	~Rocket();

	void FUpdate(float deltatime, PhysicsScene* physicsScene);

private:

	float m_fFuel;
};

