#include "Rocket.h"

#include <Circle.h>
#include <Input.h>
#include <PhysicsScene.h>
#include <iostream>

Rocket::Rocket()
{
	
}

Rocket::Rocket(float a_Fuel, glm::vec2 a_position, glm::vec2 a_velocity, float a_mass, float a_radius, glm::vec4 a_colour)
	: Circle(a_position, a_velocity, a_mass, a_radius, a_colour)
{
	m_fFuel = a_Fuel;
	m_fMass += m_fFuel;
}


Rocket::~Rocket()
{

}

void Rocket::FUpdate(float deltatime, PhysicsScene* physicsScene)
{
	aie::Input* input = aie::Input::getInstance();
	float oldFuel = m_fFuel;

	if (input->isKeyDown(aie::INPUT_KEY_P) && m_fFuel >= 0)
	{
		m_fFuel -= 0.2f;
		Circle* temp = new Circle(this->GetPosition() - glm::vec2(0, getRadius() ) , glm::vec2(0), 0.5f, 0.25f, glm::vec4(1, 1, 1, 1),1,1,1, false, true);
		physicsScene->addActor(temp);
		this->ApplyForce( glm::vec2(0, 150));
		//do rocket things
	}
	if (m_fFuel != oldFuel)
	{
		//update my mass with the new amount of fuel
		float difference = m_fFuel - oldFuel;
		m_fMass += difference;
	}

}


